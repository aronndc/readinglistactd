//1. Create a readingListActD folder. Inside create an index.html and index.js files for the first part of the activity and create a crud.js file for the second part of the activity.
//2. Once done with your solution, create a repo named 'readingListActD' and push your documents.
//3. Save the repo link on S32-C1

//Part 1:
/*
Create a readingListActD folder. Inside, create an index.html and index.js file. Test the connection of your js file to the html file by printing 'Hello World' in the console.
1.)
Create a student class sectioning system based on their entrance exam score.
If the student average is from 80 and below. Message: Your section is Grade 10 Section Ruby,
If the student average is from 81-120. Message: Your section is Grade 10 Section Opal,
If the student average is from 121-160. Message: Your section is Grade 10 Section Sapphire,
If the student average is from 161-200 to. Message: Your section is Grade 10 Section Diamond

Sample output in the console: Your score is (score). You will become proceed to Grade 10 (section)
*/


console.log('Hello world');
console.log(" ");

function studentClassSection(average) {
    let message;

    if (average <= 80) {
        message = `You got a score of ${average}. Your section is Grade 10 Section Ruby.`;
    } else if (average > 80 && average <= 120) {
        message = `You got a score of ${average}. Your section is Grade 10 Section Opal.`;
    } else if (average > 120 && average <= 160) {
        message = `You got a score of ${average}. Your section is Grade 10 Section Sapphire.`;
    } else if (average > 160 && average <= 200) {
        message = `You got a score of ${average}. Your section is Grade 10 Section Diamond.`;
    } else {
        message = `You got a score of ${average}. Sorry we can't identify your Section.`;
    }
    console.log(message);
};

studentClassSection(100);
studentClassSection(75);
studentClassSection(180);
studentClassSection(135);
studentClassSection(220);

console.log(" ");


/*
2.) 
Write a JavaScript function that accepts a string as a parameter and find the longest word within the string.

Sample Data and output:
Example string: 'Web Development Tutorial'
Expected Output: 'Development'
*/


function getLongestWord(string) {
    let stringArray = string.split(" ").sort();
    console.log(stringArray[0]);
}
getLongestWord('Web Development Tutorial');


/*
3.)
Write a JavaScript function to find the first not repeated character.

Sample arguments : 'abacddbec'
Expected output : 'e'
*/


function notRepeatedCharacted(string) {
    for (var x = 0; x < string.length; x++) {
        let onlyCharacter = string.charAt(x);
        if (string.indexOf(onlyCharacter) == x && string.indexOf(onlyCharacter, x + 1) == -1) {
            console.log(onlyCharacter);
            return;
        }
    }
};

notRepeatedCharacted('abacddbec');