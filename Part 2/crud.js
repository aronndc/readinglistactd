/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock database of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots
*/

const http = require("http");
let port = 8000;

const server = http.createServer(function(req, res) {
    if (req.url == "/" && req.method == 'GET') {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to Ordering System.");
    } else if (req.url == "/dashboard" && req.method == 'GET') {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(`Welcome to your User's Dashboard!`);
    } else if (req.url == "/products") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(`Here’s our products availability.`);
    } else if (req.url == "/addProduct" && req.method == 'POST') {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end("Add a course to our resources.");
    } else if (req.url == "/updateProduct" && req.method == 'PUT') {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end("Update a course to our resources.");
    } else if (req.url == "/archiveProduct" && req.method == 'DELETE') {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end("Archive courses to our resources.");
    } else {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end("Sorry! Page not found! :(");
    }
});

server.listen(port);
console.log(`Server is running and listening on port ${port}`);